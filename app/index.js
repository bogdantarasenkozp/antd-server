
import Koa        from 'koa';
import cors       from 'koa-cors';
import logger     from 'koa-logger';
import convert    from 'koa-convert';
import helmet     from 'koa-helmet';
import bodyParser from 'koa-bodyparser';
import serve      from 'koa-static';
import { SERVER } from './config/app.config';
import { CatchErrors } from './middlewares';

import { publicRoutes, privateRoutes } from './router';

const app = new Koa();

app
  .use(serve('.'))
  .use(CatchErrors)
  .use(helmet())
  .use(convert(cors({ origin: true })))
  .use(logger())
  .use(convert(bodyParser({ limit: '10mb' })))
  .use(publicRoutes.routes())
  .use(privateRoutes.routes())
  .listen(SERVER.port);

export default app;
